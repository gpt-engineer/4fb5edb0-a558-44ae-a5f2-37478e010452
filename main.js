document.getElementById('add-task').addEventListener('click', function() {
    var value = document.getElementById('new-task').value;
    if (value) {
        addTask(value);
        document.getElementById('new-task').value = '';
    }
});

function addTask(text) {
    var list = document.getElementById('task-list');

    var item = document.createElement('li');
    item.innerText = text;

    var buttons = document.createElement('div');

    var deleteButton = document.createElement('button');
    deleteButton.classList.add('bg-red-500', 'hover:bg-red-700', 'text-white', 'font-bold', 'py-1', 'px-2', 'rounded', 'ml-2');
    deleteButton.innerText = 'Delete';
    deleteButton.addEventListener('click', deleteTask);
    buttons.appendChild(deleteButton);

    item.appendChild(buttons);

    list.appendChild(item);
}

function deleteTask(e) {
    var item = e.target.parentNode.parentNode;
    item.parentNode.removeChild(item);
}
